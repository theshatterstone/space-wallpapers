# Space Wallpapers

This is a set of Space Wallpapers gathered from various sources,
primarily wallhaven.

## Script for getting naming right:

` n=1; for f in *.png; do mv -- "$f" "$(printf "%04d.png" $n)"; n=$((n+1)); done `

